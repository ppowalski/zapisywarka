<?php
	session_start();

	if(isset($_SESSION['login']))
	{
		Header('Location: index.php');
		exit();
	}

	if(isset($_POST['loginN']) && !empty($_POST['loginN']) && !empty($_POST['loginP'])) //logowanie
	{
		$login = $_POST['loginN'];
		$password = $_POST['loginP']; 

		try
		{
			$connection = new mysqli("localhost", "root", "", "zapisy");
			if($connection->connect_errno == 0) //udalo sie polaczyc
			{
				$login = $connection->real_escape_string($login);
				if($result = $connection->query("SELECT * FROM users WHERE login='".$login."'"))
				{
					if($result->num_rows > 0)
					{
						$row = $result->fetch_assoc();
						$hash = $row['password'];
						if(password_verify($password, $hash))
						{
							$_SESSION['login'] = $login;
							Header('Location: index.php'); 
						}
						else
						{
							echo "<script type='text/javascript'>alert('Niepoprawne hasło');</script>";
						}
						$result->free_result();
					}
					else
					{
						echo "<script type='text/javascript'>alert('Nieprawidłowe imie i nazwisko');</script>";
					}
				}
				else
				{
					throw new Exception("Nie udało się wykonać zapytania");
				}
				$connection->close();
			}
			else
			{
				throw new Exception('Wystapił błąd połączenia z baza danych');
			}
		}
		catch(Exception $error)
		{
			echo $error->getMessage();
		}
	}

	if(isset($_POST['registerN']) && !empty($_POST['registerN'])) //rejestracja
	{
		$login = $_POST['registerN'];
		$password = $_POST['registerP']; 
		$password2 = $_POST['registerP2'];
		$udano = true;

		if(strlen($password) < 3 || strlen($login) < 5)
		{
			$udano = false;
		}

		if(strcmp($password, $password2) != 0)
		{
			$udano = false;
		}

		try
		{
			$connection = new mysqli("localhost", "root", "", "zapisy");
			if($connection->connect_errno == 0) //udalo sie polaczyc
			{
				$login = $connection->real_escape_string($login);
				if($result = $connection->query("SELECT id FROM users WHERE login='".$login."'"))
				{
					if($result->num_rows > 0)
					{
						$udano = false;
					}
				}
				else
				{
					throw new Exception("Nie udało się wykonać zapytania");
				}

				if($udano == true)
				{
					$login = $connection->real_escape_string($login);
					$password = password_hash($password, PASSWORD_DEFAULT);
					if($connection->query("INSERT INTO users (login, password) VALUES ('".$login."', '".$password."')"))
					{
							$_SESSION['login'] = $login;
							Header('Location: index.php'); 
					}
					else
					{
						throw new Exception("Nie udało się wykonać zapytania");
					}
				}
				else
				{
					echo "<script type='text/javascript'>alert('Podane hasła nie są identyczne lub użytkownik już istnieje');</script>";
				}
				$connection->close();
			}
			else
			{
				throw new Exception('Wystapił błąd połączenia z bazą danych');
			}
		}
		catch(Exception $error)
		{
			echo $error->getMessage();
		}
	}
?>

<!DOCTYPE html>
<html lang="pl-PL">
<head>
	<title>Logowanie</title>
	<meta charset="UTF-8">
	<link rel="stylesheet" href="style.css" type="text/css">
</head>
<body>
	<form method="POST">
		<div class="title">Logowanie</div>
		<input type="text" name="loginN" placeholder="Imie Nazwisko"></input>
		<input type="password" name="loginP" placeholder="Hasło"></input>
		<button>Zaloguj</button>
	</form>
	<form method="POST">
		<div class="title">Rejestracja</div>
		<input type="text" name="registerN" placeholder="Imie Nazwisko"></input>
		<input type="password" name="registerP" placeholder="Hasło"></input>
		<input type="password" name="registerP2" placeholder="Powtórz hasło"></input>
		<button>Zarejestruj</button>
	</form>
</body>
</html>