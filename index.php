<?php
	session_start();

	function search($v, $tab) :int
	{
		foreach($tab as $value) 
		{
			if($v == $value)
			{
				return 1;
			}
		}
		return 0;
	}

	function getPos()
	{
		$tab = array();

		try
		{
			$connection = new mysqli("localhost", "root", "", "zapisy");
			if($connection->connect_errno == 0) //udalo sie polaczyc
			{
				if($result = $connection->query("SELECT * FROM zapisy"))
				{
					while($row = $result->fetch_assoc()) 
					{
						$tab[] = ($row['dzien']*100)+$row['godzina'];
					}
					$result->free_result();
				}
				else
				{
					throw new Exception("Nie udało się wykonać zapytania");
				}
				$connection->close();
			}
			else
			{
				throw new Exception('Wystapił błąd połączenia z baza danych');
			}
		}
		catch(Exception $error)
		{
			echo $error->getMessage();
		}
		return $tab;
	}

	function getHour(int $hour) :string
	{
		if($hour >= 10 && $hour <= 16)
		{
			return $hour.":00:00";
		}
		else if($hour >= 30 && $hour <= 36)
		{
			$hour = $hour-20;
			return $hour.":20:00"; 
		}
		else if($hour >= 50 && $hour <= 56)
		{
			$hour = $hour-40;
			return $hour.":40:00"; 
		}
	}

	function data(string $login) :string
	{
		$day="";
		try
		{
			$connection = new mysqli("localhost", "root", "", "zapisy");
			if($connection->connect_errno == 0) //udalo sie polaczyc
			{
				$login = $connection->real_escape_string($login);
				if($result = $connection->query("SELECT * FROM zapisy WHERE login='".$login."'"))
				{
					if($result->num_rows > 0)
					{
						$row = $result->fetch_assoc();
						if($row['dzien'] == 1)
						{
							$day = "Środa (06.02.2019)";
						}
						else if($row['dzien'] == 2)
						{
							$day = "Czwartek (07.02.2019)";
						}
						else if($row['dzien'] == 3)
						{
							$day = "Piątek (08.02.2019)";
						}
						$hour = getHour($row['godzina']);
						$day = $day.'<br>'.$hour;
					}
					else
					{
						$day = "BRAK";
					}
				}
				else
				{
					throw new Exception("Nie udało się wykonać zapytania");
				}
				$connection->close();
			}
			else
			{
				throw new Exception('Wystapił błąd połączenia z baza danych');
			}
		}
		catch(Exception $error)
		{
			echo $error->getMessage();
		}
		return $day;
	}

	if(!isset($_SESSION['login']))
	{
		Header('Location: login.php');
		exit();
	}

	if(isset($_POST['logout']))
	{
		unset($_SESSION['login']);
		header('Location: index.php');
	}

	if(isset($_POST['send']))
	{
		$error = false;
		$value = $_POST['send'];
		$day = floor($value/100);
		$hour = $value-($day*100);

		if($day != 1 && $day != 2 && $day != 3)
		{
			$error = true;
		}

		if(($hour < 10 && $hour > 16) && ($hour < 30 && $hour > 36) && ($hour < 50 && $hour > 56))
		{
			$error = true;
		}

		try
		{
			$connection = new mysqli("localhost", "root", "", "zapisy");
			if($connection->connect_errno == 0) //udalo sie polaczyc
			{
				$login = $_SESSION['login'];
				if($result = $connection->query("SELECT * FROM zapisy WHERE login='".$login."'"))
				{
					if($result->num_rows > 0)
					{
						$error = true; 
					}
					else
					{
						$tab = getPos();
						if(search($value, $tab) != 0)
						{
							$error = true;
						}
						if($error == false)
						{
							if($connection->query("INSERT INTO zapisy (dzien, godzina, login) VALUES ('".$day."', '".$hour."','".$login."')"))
							{
								Header('Location: index.php'); 
							}
							else
							{
								throw new Exception($connection->error);
							}
						}
						else
						{
							echo "<script type='text/javascript'>alert('Jestes juz zapisany lub podana data jest zajeta');</script>";
						}
					}
				}
				else
				{
					throw new Exception("Nie udało się wykonać zapytania");
				}
				$connection->close();
			}
			else
			{
				throw new Exception('Wystapił błąd połączenia z baza danych');
			}
		}
		catch(Exception $error)
		{
			echo $error->getMessage();
		}
	}
?>

<!DOCTYPE html>
<html lang="pl-PL">
<head>
	<title>Zapisywarka</title>
	<meta charset="UTF-8">
	<link rel="stylesheet" href="style.css" type="text/css">
</head>
<body>
	<table>
		<tr>
			<td>Godziny</td>
			<td>Środa (06.02.2019)</td>
			<td>Czwartek (07.02.2019)</td>
			<td>Piątek (08.02.2019)</td>
		</tr>
		<?php
		$tab = getPos();

		for($i=10; $i<17; $i++)
		{
			echo '<tr>';
			echo '<td>'.$i.":00:00".'</td>';

			if(search($i+100, $tab) == 0)
			{
				echo '<td><form method="POST"><button class="zapisz" value="'.($i+100).'" name="send">Zapisz</button></form></td>';
			}
			else
			{
				echo '<td><span style="color:red">Zajęte</span></td>';
			}

			if(search($i+200, $tab) == 0)
			{
				echo '<td><form method="POST"><button class="zapisz" value="'.($i+200).'" name="send">Zapisz</button></form></td>';
			}
			else
			{
				echo '<td><span style="color:red">Zajęte</span></td>';
			}

			if(search($i+300, $tab) == 0)
			{
				echo '<td><form method="POST"><button class="zapisz" value="'.($i+300).'" name="send">Zapisz</button></form></td>';
			}
			else
			{
				echo '<td><span style="color:red">Zajęte</span></td>';
			}
			echo '</tr>';

			echo '<tr>';
			echo '<td>'.$i.":20:00".'</td>';
			if(search($i+120, $tab) == 0)
			{
				echo '<td><form method="POST"><button class="zapisz" value="'.($i+120).'" name="send">Zapisz</button></form></td>';
			}
			else
			{
				echo '<td><span style="color:red">Zajęte</span></td>';
			}

			if(search($i+220, $tab) == 0)
			{
				echo '<td><form method="POST"><button class="zapisz" value="'.($i+220).'" name="send">Zapisz</button></form></td>';
			}
			else
			{
				echo '<td><span style="color:red">Zajęte</span></td>';
			}

			if(search($i+320, $tab) == 0)
			{
				echo '<td><form method="POST"><button class="zapisz" value="'.($i+320).'" name="send">Zapisz</button></form></td>';
			}
			else
			{
				echo '<td><span style="color:red">Zajęte</span></td>';
			}
			echo '</tr>';

			echo '<tr>';
			echo '<td>'.$i.":40:00".'</td>';
			if(search($i+140, $tab) == 0)
			{
				echo '<td><form method="POST"><button class="zapisz" value="'.($i+140).'" name="send">Zapisz</button></form></td>';
			}
			else
			{
				echo '<td><span style="color:red">Zajęte</span></td>';
			}

			if(search($i+240, $tab) == 0)
			{
				echo '<td><form method="POST"><button class="zapisz" value="'.($i+240).'" name="send">Zapisz</button></form></td>';
			}
			else
			{
				echo '<td><span style="color:red">Zajęte</span></td>';
			}

			if(search($i+340, $tab) == 0)
			{
				echo '<td><form method="POST"><button class="zapisz" value="'.($i+340).'" name="send">Zapisz</button></form></td>';
			}
			else
			{
				echo '<td><span style="color:red">Zajęte</span></td>';
			}
			echo '</tr>';
		}
		?>
	</table>
	<div class="info">
		Twoj termin:<br><?php echo data($_SESSION['login']); ?>
		<form method="POST">
			<button name="logout">Wyloguj</button>
		</form>
	</div>
	<div style="clear:both"></div>
</body>
</html>